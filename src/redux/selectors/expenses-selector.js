import moment from 'moment';
// применяет фильтры и возвращает только те expenses которые их прошли
const getVisibleExpenses = (expenses, {text, sortBy, startDate, endDate}) => {
    return expenses.filter(exp => {
            const createdAtMoment = moment(exp.createdAt);
            const startDateMatch = startDate ? startDate.isSameOrBefore(createdAtMoment, 'day') : true;
            const endDateMatch = endDate ? endDate.isSameOrAfter(createdAtMoment, 'day') : true;
            const textMatch = exp.description.toLowerCase().includes(text.toLowerCase());
            return startDateMatch && endDateMatch && textMatch;
        }
    ).sort((a, b) => {
        if (sortBy === 'date') {
            return a.createdAt < b.createdAt ? 1 : -1;
        }
        if (sortBy === 'amount') {
            return +a.amount < +b.amount ? 1 : -1;
        }
        else {
            console.log('no sorting filter matched at getVisibleExpenses');
        }
    });
};
export default getVisibleExpenses;