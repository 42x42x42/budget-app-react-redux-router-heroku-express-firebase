// возвращает сумму полученного массива затрат
const getTotalAmount = (expenses) => {
        const amountArray = expenses.map(exp => (+exp.amount));
        const sum = amountArray.reduce((accumulator, current) => (accumulator + current),0);
        return sum;
};
export default getTotalAmount;

