import {createStore, combineReducers, applyMiddleware, compose} from 'redux'
import thunk from 'redux-thunk';
import expensesReducer from '../reducers/expenses-Reducer';
import filterReducer from '../reducers/filters-Reducers';
import authReducer from '../reducers/auth-Reducer';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; //redux devtools настройка

export default () => {
    //createStore создает store
    //combineReducers объдиняет два и более редьюсера предварительно назначив им за какую переменную в объекте они отвечают
    const STORE = createStore(combineReducers({
            expenses: expensesReducer,
            filters: filterReducer,
            auth: authReducer
        }),
        composeEnhancers(applyMiddleware(thunk))
        // Добавлено для подключения ReduxDevTool, заменено composeEnhancers при добавлении redux-thunk для firebase
        // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
    );
    return STORE;
};

// Добавлено для подключения ReduxDevTool, заменено composeEnhancers при добавлении redux-thunk для firebase
// window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());


// -- example of state --
// const demoState = {
//     expenses: [{
//         id: 'sdfdf',
//         description: 'monthly rent',
//         note: 'final payment',
//         amount: 54500, //pennies
//         createdAt: 0
//     }],
//     filters: {
//         text: 'rent',
//         sortBy: 'amount', // date or amount
//         startDate: undefined,
//         endDate: undefined
//     }
// };