import moment from 'moment';
import database from '../../firebase/firebase';



// ADD EXPENSE
export const addExpense = (expense) => ({
    type: "ADD_EXPENSE",
    expense: expense
});
// возврат функции, а не объекта работает только при установке redux-thunk!!!
// получает expense,отправляет его в firebase, и если успех, то и в store через action addExpense с добавлением id=key из firebase
export const startAddExpense = (expensesData = {}) => {
    // dispatch и getState передает redux-thunk
    return (dispatch,getState) => {
        const uid = getState().auth.uid;
        // деструктурировать аргумент из материнской функции c назначением default values
        const {description = '', note = '', amount = '', createdAt = 0} = expensesData;
        // собрать в объект expense
        const expense = {description, note, amount, createdAt};
        // отправить expense в firebase, если успех then взять полученый ключ и отправить вместе с expense в Store
        // ref возвращается push-ем в ней есть присвоеннный key
        // return нужен для тестирования(возвращает промис), сам action работает и без него
        return database.ref(`users/${uid}/expenses`).push(expense).then(ref => {
            dispatch(addExpense({
                id: ref.key,
                ...expense
            }));
        });
    };
};


// SET EXPENSES
export const setExpenses = (expenses) => ({
    type: "SET_EXPENSES",
    expenses: expenses
});

export const startSetExpenses = () => {
    return (dispatch,getState) => {
        const uid = getState().auth.uid;
        console.log(getState());
        return database.ref(`users/${uid}/expenses`)
            .once('value')
            .then((snapshot) => {
                const expensesArray = [];

                snapshot.forEach((childSnapshot) => {
                    expensesArray.push({
                        id: childSnapshot.key,
                        ...childSnapshot.val()
                    });
                });
                dispatch(setExpenses(expensesArray));
            });
    };
};

export const removeExpense = ({id}) => {
    return {
        type: 'REMOVE_EXPENSE',
        id: id
    }
};

export const startRemoveExpense = ({id} = {}) => {
    return (dispatch,getState) => {
        const uid = getState().auth.uid;
        return database.ref(`users/${uid}/expenses/${id}`).remove().then(() => {
            dispatch(removeExpense({id}));
        });
    };
};


export const editExpense = (id, updates) => {
    return {
        type: 'EDIT_EXPENSE',
        id: id,
        updates: updates
    }
};

export const startEditExpense = (id, updates) => {
    return (dispatch,getState) => {
        const uid = getState().auth.uid;
        // update without ...
        return database.ref(`users/${uid}/expenses/${id}`).update({...updates}).then(() => {
            dispatch(editExpense(id, updates));
        });
    };
};


// -- OLD and simple addExpense without firebase
//
// параметры: деструктуризировать объект в параметрах, если его нет - создать пустой и назначить переменные по умолчанию
// export const addExpense = ({description = '', note = '', amount = '', createdAt = moment()} = {}) => {
//     return {
//         type: 'ADD_EXPENSE',
//         expense: {
//             id: uuid(),
//             description: description,
//             note: note,
//             amount: amount, //pennies
//             createdAt: createdAt
//         }
//     }
// };
