//Expenses REDUCER
const expensesDefaultState = [];
// этот reducer управляет только expenses(так мы указали в комбайн редюсерс в файле конфигСтор) к которым он был привязан в combineReducers,
// в его state находится ТОЛЬКО массив из раздела expenses, другие он даже НЕ видит
// соответственно он обновляет только раздел expenses и к другим не может получить доступ никак
const expensesReducer = (state = expensesDefaultState, action) => {
    switch (action.type) {
        case 'ADD_EXPENSE':
            return [...state, action.expense]; //т.к. менять state нельзя нужно всегда возвращать новый. spread operator(...) берет все элементы их массива state
        case 'REMOVE_EXPENSE':
            return state.filter(exp => exp.id !== action.id);
        case 'EDIT_EXPENSE':
            return state.map((expense) => {
                // если ид совпало то отредактировать expense добавив в него на перезапись новые свойства
                // если не совпало то вернуть без изменений
                if (expense.id === action.id) {
                    return {
                        ...expense,
                        ...action.updates
                    }
                } else {
                    return expense;
                }
            });
        case 'SET_EXPENSES':
            return action.expenses;
        default:
            return state
    }
};
export default expensesReducer;