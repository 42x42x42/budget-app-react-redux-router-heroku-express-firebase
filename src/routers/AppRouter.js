import React from 'react';
import {Router, Route, Switch, Link, NavLink} from 'react-router-dom';
import createHistory from 'history/createBrowserHistory'
import AddExpensePage from '../components/AddExpensePage';
import EditExpensePage from '../components/EditExpensePage';
import ExpenseDashboardPage from '../components/ExpenseDashboardPage';
import HelpPage from '../components/HelpPage';
import NotFoundPage from '../components/NotFoundPage';
import LoginPage from '../components/LoginPage';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';

export const history = createHistory(); // требует установки npm i history

const AppRouter = () => (
    <Router history={history}>
        <div>
            {/*то что не входит в switch отображается на каждой странице,тут могут быть компоненты футеры,хедеры*/}
            {/*switch ищет первое сходство в пути и останавливается на нем*/}
            <Switch>
                {/*exact=true означает что данный путь ренедерится только если урл полностью совпадает,а не одним началом на / */}
                <PublicRoute path="/" exact={true} component={LoginPage}/>
                <PrivateRoute path="/dashboard" component={ExpenseDashboardPage}/>
                <PrivateRoute path="/create" component={AddExpensePage}/>
                <PrivateRoute path="/edit/:id" component={EditExpensePage}/>
                <Route path="/help" component={HelpPage}/>
                {/*тк ниже не указать никакой путь то по умолчанию данный компонент подходит
                всем "неправильным" путям, которым не нашелся компонент выше*/}
                <Route component={NotFoundPage}/>
            </Switch>
        </div>
    </Router>
);
export default AppRouter;