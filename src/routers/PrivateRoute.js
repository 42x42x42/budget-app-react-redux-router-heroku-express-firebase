import React from 'react';
import {connect} from 'react-redux';
import {Route, Redirect} from 'react-router-dom';
import Header from '../components/Header';

// заблокировать незалогиненм пользователям вход на все страницы кроме "/" (страница Логина)
// PrivateRoute маршрутизатор-обертка вокруг Route,
// с помощью mapStateToProps получает доступ к переменным в state где хранится факт логина(isAuthenticated)
export const PrivateRoute = ({
                                 isAuthenticated,
                                 component: Component,
                                 ...rest
                             }) => (
    <Route {...rest} component={(props) => (
        isAuthenticated ? (
            <div>
                <Header/>
                <Component {...props}/>
            </div>
        ) : (<Redirect to="/"/>)

    )}/>
);
const mapStateToProps = (state) => ({
    isAuthenticated: !!state.auth.uid
});

export default connect(mapStateToProps)(PrivateRoute);

