import React from 'react';
import {connect} from 'react-redux';
import {Route, Redirect} from 'react-router-dom';

// заблокировать вход для залогиненых юзеров на страницы для незалогиненых (к примеру страничка Логина)
// PublicRoute маршрутизатор-обертка вокруг Route,
// если есть доступ возвращает нужный компонент, если нет - страницу "/"
// с помощью mapStateToProps получает доступ к переменным в state где хранится факт логина(isAuthenticated)
export const PublicRoute = ({
                                isAuthenticated,
                                component: Component,
                                ...rest
                            }) => (
    <Route {...rest} component={(props) => (
        isAuthenticated ? (
            <Redirect to="/dashboard"/>
        ) : ( <Component {...props}/>)

    )}/>
);
const mapStateToProps = (state) => ({
    isAuthenticated: !!state.auth.uid
});

export default connect(mapStateToProps)(PublicRoute);

