import React from 'react';
import {NavLink, Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {startLogout} from '../redux/actions/auth-Action'


export const Header = ({startLogout}) => (
    <header className="header">
        <div className="content-container">
            <div className="header__content">
                <Link className="header__title" to="/dashboard">
                    <h1>Expensify</h1>
                </Link>
                <button onClick={startLogout} className="button button--link">Logout</button>
            </div>
        </div>
    </header>
);

const mapDispatchToProps = (dispatch) => ({
    startLogout: () => dispatch(startLogout())
});

export default connect(undefined, mapDispatchToProps)(Header);


// // OLD HEADER WITH NAVLINK AS AN EXAMPLE
// <header>
//     <h1>Expensify</h1>
//     {/*Link позволяет сменять страницы без фактического ре-рендеринга страницы
//         exact={true} - проверяет на точное совпадение адреса страницы(актуально для "/"
//         */}
//     <NavLink to="/dashboard" activeClassName="is-active">Home </NavLink>
//     <NavLink to="/create" activeClassName="is-active">Create </NavLink>
//     {/*<NavLink to="/edit" activeClassName="is-active">Edit </NavLink>*/}
//     <NavLink to="/help" activeClassName="is-active">Help </NavLink>
//     <button onClick={startLogout}>Logout</button>
// </header>