import React from 'react';
import {connect} from 'react-redux';
import ExpenseListItem from './ExpenseListItem';
import selectExpenses from '../redux/selectors/expenses-selector';

// список Expenses
export const ExpenseList = (props) => (
    <div className="content-container">
        <div className="list-header">
            <div className="show-for-mobile">Expense</div>
            <div className="show-for-desktop">Expense</div>
            <div className="show-for-desktop">Amount</div>
        </div>
        <div className="list-body">
            {
                props.expenses.length === 0 ? (
                    <div className="list-item list-item--message">
                        <span>No expenses</span>
                    </div>
                ) : (
                    /*данные в пропс передаются в функции connect ниже по коду*/
                    props.expenses.map(exp => ( <ExpenseListItem {...exp} key={exp.id}/> ))
                )
            }
        </div>
    </div>
);
// mapStateToProps принимает state(он же store) автоматически(функция connect это делает ниже), из него он выбирает нужные данные и возвращает объект с ними
const mapStateToProps = (state) => {
    return {
        // selectExpenses возвращает нужные expenses исходя из заданных фильтров
        expenses: selectExpenses(state.expenses, state.filters)
    };
};

// в первых скобках функция которая возвращает данные,
// в последних скобках компонент в который нужно передать данные(они будут переданы в его props)
const ConnectedExpenseList = connect(mapStateToProps)(ExpenseList); // когда store меняется компонент автоматически будет перерендерен
// !так же connect автоматически добавляет в props функцию dispatch!

export default ConnectedExpenseList; // ConnectedExpenseList является higher order component