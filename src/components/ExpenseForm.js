import React from 'react';
import {SingleDatePicker} from 'react-dates';
import moment from 'moment';

// компонент который позволяет заполнять данные для создания нового expense или редактирования старого
export default class ExpenseForm extends React.Component {
    // стиль написания кода как ниже требует установки babel-plugin-transform-class-properties
    constructor(props) {
        super(props);
        this.state = {
            description: props.expense ? props.expense.description : '',
            note: props.expense ? props.expense.note : '',
            amount: props.expense ? props.expense.amount : '',
            createdAt: props.expense ? moment(props.expense.createdAt) : moment(),
            calendarFocused: false,
            error: ''
        };
    };

    onDescriptionChanged = (e) => {
        const description = e.target.value;
        this.setState(() => ({description: description}));
    };
    onAmountChange = (e) => {
        const amount = e.target.value;
        // reg exp позволяет ввести либо только цифры либо цифры с точкой и 0-2 цифрами после нее
        // тк value формы управляется через стейт,то в случае несоотвествия reg exp неподходящий символ не будет введен в форму
        if (!amount || amount.match(/^\d{1,}(\.\d{0,2})?$/)) {
            this.setState({amount: amount});
        }
    };
    onDateChange = (createdAt) => {
        if (createdAt) {
            this.setState({createdAt: createdAt});
        }

    };

    onFocusChange = ({focused}) => {
        this.setState(() => ({calendarFocused: focused}));
    };


    onTextAreaChange = e => {
        const note = e.target.value;
        this.setState(() => ({note: note}));
    };
    onSubmit = e => {
        e.preventDefault();

        if (!this.state.description || !this.state.amount) {
            //error message
            this.setState({error: 'please add description and amount'})
        } else {
            this.setState({error: ''});// error message спрятать
            console.log('submited');
            this.props.onSubmit({
                description: this.state.description,
                amount: this.state.amount,
                note: this.state.note,
                createdAt: this.state.createdAt.valueOf()
            })
        }
    };

    render() {
        return (

                <form onSubmit={this.onSubmit} className="form">
                    {this.state.error && <p className="form__error">{this.state.error}</p>}
                    <input
                        className="text-input"
                        onChange={this.onDescriptionChanged}
                        value={this.state.description}
                        type="text"
                        placeholder="description"
                        autoFocus/>
                    <input
                        className="text-input"
                        onChange={this.onAmountChange}
                        type="text"
                        value={this.state.amount}
                        placeholder="amount"/>
                    <SingleDatePicker
                        date={this.state.createdAt}
                        onDateChange={this.onDateChange}
                        focused={this.state.calendarFocused}
                        onFocusChange={this.onFocusChange}
                        // выше обязательные функции, ниже нет
                        numberOfMonths={1} // кол-во месяцев в выпадающем календаре
                        isOutsideRange={() => false} // Разрешает выбирать дату ранее текущей
                    />
                    <textarea
                        className="textarea"
                        onChange={this.onTextAreaChange}
                        value={this.state.note}
                        placeholder="add notes(optional)">
                    </textarea>
                    <div>
                        <button className="button">Save expense</button>
                    </div>
                </form>
        )
    }
}

