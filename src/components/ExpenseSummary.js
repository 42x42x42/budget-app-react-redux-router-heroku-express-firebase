import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import numeral from 'numeral';
import getTotalAmount from '../redux/selectors/expenses-total';
import getVisibleExpenses from '../redux/selectors/expenses-selector';


export const ExpensesSummary = (props) => {
    const expenseWord = props.expenses.length === 1 ? 'expense' : 'expenses';
    const totalAmount = numeral(getTotalAmount(props.expenses)).format('$0,0.00')
    return (
        <div className="page-header">
            <div className="content-container">
                <h1 className="page-header__title">
                    You are viewing <span>{props.expenses.length}</span> {expenseWord}.Total <span>{totalAmount}</span>
                </h1>
                <div className="page-header__action">
                    <Link to="/create" className="button">Add Expense</Link>
                </div>
            </div>
        </div>)
};

const mapStateToProps = (state) => ({
    expenses: getVisibleExpenses(state.expenses, state.filters)
});

const ConnectedExpensesSummary = connect(mapStateToProps)(ExpensesSummary);
export default ConnectedExpensesSummary;
