import React from 'react';
import {connect} from 'react-redux';
import ExpenseForm from './ExpenseForm';
import {startAddExpense} from '../redux/actions/expenses-Actions'

export class AddExpensePage extends React.Component {
    //финальный "сборный" onSumbit который в итоге будет передан в ExpenseForm
    onSubmit = (expense) => {
        this.props.startAddExpense(expense);
        this.props.history.push('/'); // пересылает на заданную страницу,в данном случае это стартовая
    };

    render() {
        return (
            <div>
                <div className="page-header">
                    <div className="content-container">
                        <h1 className="page-header__title">Add Expense</h1>
                    </div>
                </div>
                <div className="content-container">
                    <ExpenseForm onSubmit={this.onSubmit}/>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
//Этот startAddExpense попадет в пропсы и станет частью onSubmit внутри AddExpense, и дальше будет прокинут в ExpenseForm
    startAddExpense: (expense) => dispatch(startAddExpense(expense))
});
// если доступ на чтение store не нужен то в первую переменную можно не помещать никакую функцию
// во вторую переменную можно помещать функцию которая возвращает объект внутри которого функции,
// которые нужно добавить в пропсы с использованием диспатч
export default connect(undefined, mapDispatchToProps)(AddExpensePage);


/*
// OLD CODE NOT SUPPORTED BY JEST TESTING


//inside of props is dispatch function
const AddExpensePage = (props) => (
    <div>
        this is AddExpensePage
        <ExpenseForm
            //т.к. данная форма reusable то onSubmit прописан не внутри нее,а передается как пропс
            onSubmit={(expense) => {
                // props.dispatch(addExpense(expense)); // вариант без mapDispatchToProps
                props.onSubmit(expense);
                props.history.push('/'); // пересылает на заданную страницу,в данном случае это стартовая
            }}
        />
    </div>
);

const mapDispatchToProps = (dispatch) => ({
    onSubmit: (expense) => dispatch(addExpense(expense))
});

// если доступ на чтение store не нужен то в первую переменную можно не помещать никакую функцию
// во вторую переменную можно помещать функцию которая возвращает объект внутри которого функции,
// которые нужно добавить в пропсы с использованием диспатч
export default connect(undefined, mapDispatchToProps())(AddExpensePage);
*/