// SIMPLE REDUX COUNTER




//npm i redux
//npm install --save-dev babel-plugin-transform-object-rest-spread +настройка плагина(см. инструкцию к плагину)
import {createStore} from 'redux';

// ------ACTIONS
// объект который попадет в параметры деструктуризировать, достать из него поле incrementBy,
// если объекта нет, по дефолту задать пустой, если в объекте нет поля incrementBy, то по дефолту incrementBy =1
// возвращается новый объект с полем type и incrementBy (всега равным 1 или переданному значению)
// принимает объект и пустоту
const incrementBy = ({incrementBy = 1} = {}) => ({
    type: 'INCREMENT',
    incrementBy: incrementBy
});
// та же логика что и выше в incrementBy,только чуть меньше es6 в коде
// принимает объект, если объекта нет,то создает по дефолту пустой
const decrementBy = (obj = {}) => {
    //деструктурировать, достать декремент, если поля нет задать по умолчанию 1
    const {decrementBy = 1} = obj;
    // const decrementBy = obj.decrementBy || 1; //аналогично записи выше, только не умеет принимать дефолтный прамент
    return {
        type: 'DECREMENT_NEW',
        decrementBy: decrementBy
    }
};
const setCount = ({count}) => ({
    type: 'SET',
    count: count
});
const resetCount = () => ({
    type: 'RESET'
});
// -----REDUCER
// 1 pure function никогда не взаимодействует с переменными вне самого редюсера
// 2 никогда не меняет стейт или экшн, только возвращает новый объект state
const countReducer= (state = {count: 0}, action) => {
    switch (action.type) {
        case 'INCREMENT':
            //проверяет есть ли поле incrementBy, если да, то взять его, его нет, задать по умолчанию incrementBy=1
            const increment = typeof(action.incrementBy) === 'number' ? action.incrementBy : 1;
            return {count: state.count + increment};
        case 'INCREMENT_NEW':
            return {count: state.count + action.incrementBy};
        //
        case 'DECREMENT':
            //проверяет есть ли поле decrementBy, если да, то взять его, его нет, задать по умолчанию decrementBy=1
            const decrement = typeof(action.decrementBy) === 'number' ? action.decrementBy : 1;
            return {count: state.count - decrement};
        case 'DECREMENT_NEW':
            return {count: state.count - action.decrementBy};
        //
        case 'RESET':
            return {count: 0};
        case 'SET':
            return {count: action.count};
        default:
            return state
    }
};

// ----STORE
//принимает внутрь редюсер
const STORE = createStore(countReducer);

// ---LISTENERS
//если вызвать unsubscibe() то автоматически все слушатели отпишутся т.к. subscribe после подписи возвращает функцию "отписчик"
const unsubscibe = STORE.subscribe(() => console.log(STORE.getState().count));

// вызывает изменения в предыдущем сторе, принимает action внутри которого объект с string описывающий этот action и возможные доп.параметры
STORE.dispatch({type: 'INCREMENT', incrementBy: 5});
STORE.dispatch({type: 'DECREMENT'});
STORE.dispatch({type: 'DECREMENT'});
STORE.dispatch({type: 'RESET'});
STORE.dispatch({type: 'DECREMENT', decrementBy: 10});
STORE.dispatch({type: 'SET', count: 101});
//new way with functions as commands
STORE.dispatch(incrementBy({incrementBy: 50}));
STORE.dispatch(decrementBy({decrementBy: 3}));
STORE.dispatch(decrementBy()); // отнимет 1
STORE.dispatch(setCount({count: 999}));
STORE.dispatch(resetCount());

