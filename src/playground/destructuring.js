//OBJECT DESTRUCTURING
const person = {
    name: 'Dima',
    age: 29,
    location: {
        city: 'Kyiv',
        country: 'Ukraine'
    }
};
//имена должны совпадать
const {name,age} = person;
console.log(`name is ${name} and age is ${age}`);
// coutry: state переименовует переменную на state, citizenship='unknown' задает параметр по дефолту если такой не найдет в объекте
const {city,country: state, citizenship = 'unknown'}=person.location;
console.log(`Person lives in a state of ${state} in city ${city} and his citizenship is ${citizenship}`);



//ARRAY DESTRUCTURING

const address = ['12',' 23','Kroisberg','Berlin'];
//т.к нет имен элементы присваиваются по порядку, если нужно что-то пропустить то просто оставить пустое место
// между(или перед) запятыми чтоб элемент прпоустило, по каждый ненужный элемент должно быть пустое место
// const [ appartment, house,street,town]= address; // все ячейки назвали
const [ , ,street,town]= address; // только 2 и 4 назвали, если есть 5,6,7 и тд они игнорируются
console.log(`I live in a city of ${town} in a street ${street}`);