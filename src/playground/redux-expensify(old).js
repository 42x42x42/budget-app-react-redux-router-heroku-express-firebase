import {createStore, combineReducers} from 'redux';
import uuid from 'uuid';

//EXPENSE ACTIONS
//add expense
const addExpense = ({description = '', note = '', amount = '', createdAt = 0} = {}) => {
    return {
        type: 'ADD_EXPENSE',
        expense: {
            id: uuid(),
            description: description,
            note: note,
            amount: amount, //pennies
            createdAt: createdAt
        }
    }
};
const removeExpense = ({id}) => {
    return {
        type: 'REMOVE_EXPENSE',
        id: id
    }
};
const editExpense = (id, updates) => {
    return {
        type: 'EDIT_EXPENSE',
        id: id,
        updates: updates
    }
};

//Expenses REDUCER
const expensesDefaultState = [];
// этот reducer управляет только expenses к которым он был привязан в combineReducers,
// в его state находится ТОЛЬКО массив из раздела expenses, другие он даже НЕ видит
// соответственно он обновляет только раздел expenses и к другим не может получить доступ никак
const expensesReducer = (state = expensesDefaultState, action) => {
    switch (action.type) {
        case 'ADD_EXPENSE':
            return [...state, action.expense]; //т.к. менять state нельзя нужно всегда возвращать новый. spread operator(...) берет все элементы их массива state
        case 'REMOVE_EXPENSE':
            return state.filter(exp => exp.id !== action.id);
        case 'EDIT_EXPENSE':
            // перебрать стейт, найти экспенс где совпадает ид
            return state.map((exp) => {
                if (exp.id === action.id) {
                    console.log('match' + exp.id);
                    return {
                        ...state,
                        ...action.updates // достанет поля которые там есть и перезапишет такие же поля в стейте
                    }
                }
                else {
                    console.log('didnt match');
                    return state
                }
            });
        default:
            return state
    }
};


//-----

// filter ACTIONS
const setTextFilter = (text = '') => ({
    type: 'SET_TEXT_FILTER',
    text: text
});
const sortByAmount = () => ({
    type: 'SORT_BY_AMOUNT'
});
const sortByDate = () => ({
    type: 'SORT_BY_DATE'
});
const setStartDate = (date = undefined) => ({
    type: 'SET_START_DATE',
    startDate: date

});
const setEndDate = (date = undefined) => ({
    type: 'SET_END_DATE',
    endDate: date

});

//---Filter REDUCER
const filterDefaultState = {
    text: '',
    sortBy: 'date',
    startDate: undefined,
    endDate: undefined
};
const filterReducer = (state = filterDefaultState, action) => {
    switch (action.type) {
        case 'SET_TEXT_FILTER':
            return {
                ...state,
                text: action.text
            };
        case 'SORT_BY_AMOUNT':
            return {
                ...state,
                sortBy: 'amount'
            };
        case 'SORT_BY_DATE':
            return {
                ...state,
                sortBy: 'date'
            };
        case 'SET_START_DATE':
            return {
                ...state,
                startDate: action.startDate
            };
        case 'SET_END_DATE':
            return {
                ...state,
                endDate: action.endDate
            };
        default:
            return state;
    }
};

// sort visible expenses
const getVisibleExpenses = (expenses, {text, sortBy, startDate, endDate}) => {
    return expenses.filter(exp => {
            const startDateMatch = typeof startDate !== 'number' || exp.createdAt >= startDate;
            const endDateMatch = typeof endDate !== 'number' || exp.createdAt <= endDate;
            const textMatch = exp.description.toLowerCase().includes(text.toLowerCase());
            return startDateMatch && endDateMatch && textMatch;
        }
    ).sort((a, b) => {
        if (sortBy === 'date') {
            return a.createdAt > b.createdAt ? 1 : -1;
        }
        if (sortBy === 'amount') {
            return +a.amount < +b.amount ? 1 : -1;
        }
        else {
            console.log('no sorting filter matched at getVisibleExpenses');
        }
    });
};

//----STORE-----
const STORE = createStore(combineReducers({
        expenses: expensesReducer,
        filters: filterReducer
    }
));

const unsubscibe = STORE.subscribe(() => {
    const state = STORE.getState();
    const visibleExpenses = getVisibleExpenses(state.expenses, state.filters);
    console.log(visibleExpenses);
});


const el1 = STORE.dispatch(addExpense({description: 'first payment', amount: '100', createdAt: 500}));
const el2 = STORE.dispatch(addExpense({description: 'second payment', amount: '1200', createdAt: 1500}));
const el3 = STORE.dispatch(addExpense({description: 'third payment', amount: '150', createdAt: 1300}));
const el4 = STORE.dispatch(addExpense({description: 'forth payment', amount: '700', createdAt: -200}));

// STORE.dispatch(removeExpense({id: el1.expense.id}));
// STORE.dispatch(removeExpense({id: el2.expense.id}));
//
// STORE.dispatch(editExpense(el3.expense.id, {amount: 500}));
// // STORE.dispatch(setTextFilter('SEcond'));
STORE.dispatch(sortByAmount());
// STORE.dispatch(sortByDate());
//
// STORE.dispatch(setEndDate(2000));
// STORE.dispatch(setStartDate(125));





const demoState = {
    expenses: [{
        id: 'sdfdf',
        description: 'monthly rent',
        note: 'final payment',
        amount: 54500, //pennies
        createdAt: 0
    }],
    filters: {
        text: 'rent',
        sortBy: 'amount', // date or amount
        startDate: undefined,
        endDate: undefined
    }
};
// spread operator with objects example
// const obj = {
//     name: 'Mike',
//     gender: 'male'
//     age: 28,
// };
//
// const obj2 = {...obj, age: 29, job: 'teacher'}; // age replaced, job added
// console.log(obj2);
