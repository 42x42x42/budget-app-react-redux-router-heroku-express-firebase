import React from 'react';
import ReactDOM from 'react-dom';

//usual stateless component
const Info = (props) => (
    <div>
        <h1>title</h1>
        <p>this is a {props.info}</p>
    </div>
);

// ! this is Higher Order Component(HOC) !
const AdminWrapper = (WrappedObject) => {
    return (props) => (
        <div>
            {props.show && <p>additional text</p>}
            <WrappedObject {...props}/>
        </div>
    );
};
//вызов обертки, которая возвращает новый компонент
const AfterWrapper = AdminWrapper(Info);


ReactDOM.render(<AfterWrapper show={true} info='props.info'/>, document.getElementById('app'));


// const AuthWrapper = (WrappedObject)=>{
//     return (props)=>(
//         <div>
//             <WrappedObject {...props}/>
//             {props.show && <p>this information is added from wrapper</p>}
//         </div>
//     )
// };
// const AuthInfo = AuthWrapper(Info);
//
// ReactDOM.render(<AuthInfo info='auth wrapper info' show={true}/>, document.getElementById('app'));