import {createStore, combineReducers} from 'redux';



//-----




const el1 = STORE.dispatch(addExpense({description: 'first payment', amount: '100', createdAt: 500}));
const el2 = STORE.dispatch(addExpense({description: 'second payment', amount: '1200', createdAt: 1500}));
const el3 = STORE.dispatch(addExpense({description: 'third payment', amount: '150', createdAt: 1300}));
const el4 = STORE.dispatch(addExpense({description: 'forth payment', amount: '700', createdAt: -200}));

// STORE.dispatch(removeExpense({id: el1.expense.id}));
// STORE.dispatch(removeExpense({id: el2.expense.id}));
//
// STORE.dispatch(editExpense(el3.expense.id, {amount: 500}));
// // STORE.dispatch(setTextFilter('SEcond'));
STORE.dispatch(sortByAmount());
// STORE.dispatch(sortByDate());
//
// STORE.dispatch(setEndDate(2000));
// STORE.dispatch(setStartDate(125));





const demoState = {
    expenses: [{
        id: 'sdfdf',
        description: 'monthly rent',
        note: 'final payment',
        amount: 54500, //pennies
        createdAt: 0
    }],
    filters: {
        text: 'rent',
        sortBy: 'amount', // date or amount
        startDate: undefined,
        endDate: undefined
    }
};
// spread operator with objects example
// const obj = {
//     name: 'Mike',
//     gender: 'male'
//     age: 28,
// };
//
// const obj2 = {...obj, age: 29, job: 'teacher'}; // age replaced, job added
// console.log(obj2);
