import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux'; // help to provide single store to all app components
import AppRouter, {history} from './routers/AppRouter';
import configureStore from './redux/store/configureStore';
import {startSetExpenses} from './redux/actions/expenses-Actions';
import {login, logout} from './redux/actions/auth-Action';
import getVisibleExpenses from './redux/selectors/expenses-selector';
import {firebase} from './firebase/firebase';
import 'normalize.css/normalize.css';
// react-dates@12.7.0 (!!именно эта версия)для работы с календарем, библиотека от эирбнб,требует так же установить npm i react-addons-shallow-compare
// и импортировать import 'react-dates/lib/css/_datepicker.css';
import 'react-dates/lib/css/_datepicker.css';
import './styles/styles.scss';
import LoadingPage from './components/LoadingPage';


const appRoot = document.getElementById("app");
const STORE = configureStore();
const unsubscibe = STORE.subscribe(() => {
    const state = STORE.getState();
    const visibleExpenses = getVisibleExpenses(state.expenses, state.filters);
});

// достаточно просто обернуть основной компонент в Провайдер указав ему в параметры ссылку на Store объявленый выше
// и этого достаточно чтоб у всех компонентов был общий Стор
// дальше в каждом компоненте где нужен доступ к Стору необходимо вызывать функцию connect
const jsx = (
    <Provider store={STORE}>
        <AppRouter/>
    </Provider>
);

let hasRendered = false;
const renderApp = () => {
    if (!hasRendered) {
        ReactDOM.render(jsx, appRoot);
        hasRendered = true;
    }
};
// экран загрузки
ReactDOM.render(<LoadingPage/>, appRoot);

// после ответа от firebase грузится экран с логином либо пользовательскими данными
firebase.auth().onAuthStateChanged((user) => {
    if (user) {
        console.log('Log IN');
        // страница юзера
        STORE.dispatch(login(user.uid)); // uid передает firebase самостоятельно
        STORE.dispatch(startSetExpenses()).then(() => {
            renderApp();
            // если логин прозошел со страницы "логин" она же "/" только тогда пересылать на /dashboard
            if (history.location.pathname === '/') {
                history.push('/dashboard');
            }
        });
    } else {
        console.log('Log out');
        STORE.dispatch(logout());
        // если юзер не залагонине то показать ему старинцу логина по адресу "/"
        renderApp();
        history.push('/');
    }
});



