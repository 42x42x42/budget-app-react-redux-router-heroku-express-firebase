// import moment from "moment"; // так нельзя импортировать -stacj trace error
const moment = require.requireActual('moment');


//подменяет функцию из оригинального moment
// на примере ExpenseFOrm при вызове момент() без параметров
// оригинальная функция создает текущую дату, данная же создает дату под 0,
//для того чтоб можно было сравнивать спепшоты созданные в разное время
export default (timestamp = 0) => {
    return moment(timestamp);
}