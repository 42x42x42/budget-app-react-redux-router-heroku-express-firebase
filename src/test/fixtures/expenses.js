import moment from 'moment';

const expensesExample = [{
    description: 'car parts',
    id: '11',
    note: 'final',
    amount: 54500,
    createdAt: moment(0).subtract(4, 'days').valueOf()
}, {
    description: 'internet',
    id: '12',
    note: 'June',
    amount: 5500,
    createdAt: moment(0).add(2, 'days').valueOf()
}, {
    description: 'phone',
    id: '13',
    note: 'June',
    amount: 7000,
    createdAt: moment(0).add(3, 'days').valueOf()
}];

export default expensesExample