import React from 'react';
// import ReactShallowRenderer from 'react-test-renderer/shallow'; //enzyme заменяет
import {Header} from '../../components/Header';
import {shallow} from 'enzyme'
// после настройки в jest.config "snapshotSerializers": "enzyme-to-json/serializer" автоматически будет в Json конвертировать
// import toJson from "enzyme-to-json";

test('check Header component rendering',()=>{
    // //old jest way
    // const renderer = new ReactShallowRenderer();
    // renderer.render(<Header/>);
    // expect(renderer.getRenderOutput()).toMatchSnapshot();

    //usual test with enzyme (not snapshot)
    // const wrapper = shallow(<Header/>);
    // expect(wrapper.find('h1').text()).toBe("Expensify");

    // new enzyme way with snapshot
    const wrapper = shallow(<Header startLogout={()=> {} }/>);
    //после установки enzyme-to-json и настройки в jest.config "snapshotSerializers": "enzyme-to-json/serializer" автоматически будет в Json конвертировать
    // expect(toJson(wrapper)).toMatchSnapshot();
    expect(wrapper).toMatchSnapshot();
});

test('should call startLogout on button click',()=>{
    const startLogout = jest.fn();
    const wrapper = shallow(<Header startLogout={startLogout}/>);
    wrapper.find('button').simulate('click');
    expect(startLogout).toHaveBeenCalled();
});