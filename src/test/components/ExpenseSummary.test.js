import React from 'react';
import {shallow} from 'enzyme';
import {ExpensesSummary} from '../../components/ExpenseSummary';
import expenses from '../fixtures/expenses';

test('should match snapshot with List of expenses',()=>{
    const component = shallow(<ExpensesSummary expenses={expenses}/>);
    expect(component).toMatchSnapshot();
});

test('should match snapshot with Single expenses',()=>{
    const component = shallow(<ExpensesSummary expenses={[expenses[0]]}/>);
    expect(component).toMatchSnapshot();
});