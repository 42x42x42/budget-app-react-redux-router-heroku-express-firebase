import getTotalAmount from '../../../redux/selectors/expenses-total';
import expenses from '../../fixtures/expenses';

test("should work with Multiple expense",()=>{
    const sum = getTotalAmount(expenses);
   expect(sum).toEqual(67000);
});

test("should work with Single expense",()=>{
    const sum = getTotalAmount([expenses[2]]);
    expect(sum).toEqual(7000);
});

test("should return 0 with expense zero length []",()=>{
    const sum = getTotalAmount([]);
    expect(sum).toEqual(0);
});
