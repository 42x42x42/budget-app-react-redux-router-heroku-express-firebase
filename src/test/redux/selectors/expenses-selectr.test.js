import moment from 'moment';
import getVisibleExpenses from '../../../redux/selectors/expenses-selector'

const expensesExample = [{
    description: 'car parts',
    id: '11',
    note: 'final',
    amount: 54500,
    createdAt: moment(0).subtract(4,'days')
}, {
    description: 'internet',
    id: '12',
    note: 'June',
    amount: 5500,
    createdAt: moment(0).add(2,'days')
}, {
    description: 'phone',
    id: '12',
    note: 'June',
    amount: 7000,
    createdAt: moment(0).add(3,'days')
}];



test('filter by text (and date by default)', () => {
    const filter = {
        text: 'e',
        sortBy: 'date',
        startDate: undefined,
        endDate: undefined
    };
    const result = getVisibleExpenses(expensesExample, filter);
    expect(result).toEqual([expensesExample[2], expensesExample[1]]);
});


test('filter by startDate (and sort by date by default)', () => {
    const filter = {
        text: '',
        sortBy: 'date',
        startDate: moment(0),
        endDate: undefined
    };
    const result = getVisibleExpenses(expensesExample, filter);
    expect(result).toEqual([expensesExample[2], expensesExample[1]]);
});



test('filter by endDate (and sort by date by default)', () => {
    const filter = {
        text: '',
        sortBy: 'date',
        startDate: undefined,
        endDate: moment(0)
    };
    const result = getVisibleExpenses(expensesExample, filter);
    expect(result).toEqual([expensesExample[0]]);
});


test('sort by amount|description', () => {
    const filter = {
        text: '',
        sortBy: 'amount',
        startDate: undefined,
        endDate: undefined
    };
    const result = getVisibleExpenses(expensesExample, filter);
    expect(result).toEqual([expensesExample[0], expensesExample[2],expensesExample[1]]);
});

