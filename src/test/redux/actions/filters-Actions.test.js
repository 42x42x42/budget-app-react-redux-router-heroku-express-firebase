import moment from 'moment';
import {setStartDate,setEndDate,sortByAmount, sortByDate,setTextFilter} from '../../../redux/actions/filters-Actions';

test('should return START date action', () => {
    const action = {
        type: 'SET_START_DATE',
        startDate: moment(0)
    };
    expect(action).toEqual(setStartDate(moment(0)))
});

test('should return END date action', () => {
    const action = {
        type: 'SET_END_DATE',
        endDate: moment(0)
    };
    expect(action).toEqual(setEndDate(moment(0)))
});

test('should set text filter', () => {
    const text = {
        type: 'SET_TEXT_FILTER',
        text: 'test'
    };
    expect(text).toEqual(setTextFilter('test'));
});
test('should set text filter DEFAULT empty', () => {
    const text = {
        type: 'SET_TEXT_FILTER',
        text: ''
    };
    expect(text).toEqual(setTextFilter());
});

test('should return sort by amount', () => {
    expect(sortByAmount()).toEqual({type: 'SORT_BY_AMOUNT'})
});
test('should return sort by date', () => {
    expect({type: 'SORT_BY_DATE'}).toEqual(sortByDate())
});

