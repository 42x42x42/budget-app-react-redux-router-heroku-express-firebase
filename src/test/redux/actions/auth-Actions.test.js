import {logout, login} from '../../../redux/actions/auth-Action';


test("should generate LOGIN object-action", () => {
    const id = '123abc';
    const action = login(id);
    expect(action).toEqual({
        type: "LOGIN",
        uid: id
    });
});

test("should generate LOGOUT object-action", () => {
    const action = logout();
    expect(action).toEqual({
        type: "LOGOUT"
    });

});