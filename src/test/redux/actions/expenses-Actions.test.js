import {
    addExpense,
    startAddExpense,
    setExpenses,
    startSetExpenses,
    editExpense,
    startEditExpense,
    removeExpense,
    startRemoveExpense,
} from '../../../redux/actions/expenses-Actions';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import expenses from '../../fixtures/expenses';
import database from '../../../firebase/firebase';

const createMockStore = configureMockStore([thunk]); // thunk is middleWare
const uid = 'this_is_test_uid';
const defaultAuthState = {auth:{uid:uid}};

// перед выполнением любого из тестов: затирается база, перебирается образец expenses и наполнить им тестовую базу
beforeEach((done) => {
    const expensesData = {};
    expenses.forEach(({id, description, note, amount, createdAt}) => {
        expensesData[id] = {description, note, amount, createdAt}
    });
    database.ref(`users/${uid}/expenses`).set(expensesData).then(done());    // then(done()) заставляет ждать выполнения кода выше и только потом идти вниз
});


test('should add expense action setup, return obj with PROVIDED value', () => {
    const action = addExpense(expenses[1]);

    expect(action).toEqual({
        type: "ADD_EXPENSE",
        expense: expenses[1]
    });
});

test('should add expense to database and store', (done) => {
    const store = createMockStore(defaultAuthState); // create mock store only for this test
    const expenseData = {
        description: 'Mouse',
        amount: 3000,
        note: 'This one is better',
        createdAt: 1000
    };
    store.dispatch(startAddExpense(expenseData)).then(() => {
        const actions = store.getActions();
        //стор хранит все actions с которыми он был вызыван, выше по цепочке он был 1 раз вызван
        //т.к. вызов был первым(и единственным ) он хранится в массиве под индексом 0 (получение через store.getActions();)
        expect(actions[0]).toEqual({
            type: "ADD_EXPENSE",
            expense: {
                id: expect.any(String),
                ...expenseData
            }
        });
        database.ref(`users/${uid}/expenses/${actions[0].expense.id}`)
            .once('value')
            .then((snapshot) => {
                expect(snapshot.val()).toEqual(expenseData);
                // 'done' is a test argument and a stop point witch wait till async code above it will be executed.
                // Should be inside each async test function
                done();
            });
    });
});

test('should add expense to database and store with DEFAULT', (done) => {
    const store = createMockStore(defaultAuthState); // create mock store only for this test

    const expenseDataDefault = {
        description: '',
        note: '',
        amount: '',
        createdAt: 0
    };
    // внутрь startAddExpense ничего не передано
    store.dispatch(startAddExpense({})).then(() => {
        //стор хранит все actions с которыми он был вызыван, выше по цепочке он был 1 раз вызван
        const actions = store.getActions();
        //т.к. вызов был первым(и единственным) он хранится в массиве под индексом 0
        expect(actions[0]).toEqual({
            type: "ADD_EXPENSE",
            expense: {
                id: expect.any(String),
                ...expenseDataDefault,
            }
        });
        database.ref(`users/${uid}/expenses/${actions[0].expense.id}`)
            .once('value')
            .then((snapshot) => {
                expect(snapshot.val()).toEqual(expenseDataDefault);
                // 'done' is a test argument and a stop point witch wait till async code above it will be executed.
                // Should be inside each async test function
                done();
            });
    });
});

test("should setup set expenses action object with provided data", () => {
    const action = setExpenses(expenses);
    expect(action).toEqual({
        type: "SET_EXPENSES",
        expenses: expenses,
    });
});

test("should fetch the expenses data from firebase.", (done) => {
    const mockStore = createMockStore(defaultAuthState);
    mockStore.dispatch(startSetExpenses()).then(() => {
        const action = mockStore.getActions();
        expect(action[0]).toEqual({
            type: "SET_EXPENSES",
            expenses
        });
        done();
    });
});


test('edit expense,return obj={type,id,updates}', () => {
    const action = editExpense('123id', {note: 'some note'});
    expect(action).toEqual({
        type: 'EDIT_EXPENSE',
        id: '123id',
        updates: {
            note: 'some note'
        }
    });
});

test('should edit expense in firebase', (done) => {
    const mockStore = createMockStore(defaultAuthState);
    const id = expenses[0].id;
    const updates = {amount: 121};
    mockStore.dispatch(startEditExpense(id, updates)).then(() => {
        const action = mockStore.getActions();
        expect(action[0]).toEqual({
            type: "EDIT_EXPENSE",
            id,
            updates
        });
        return database.ref(`users/${uid}/expenses/${id}`).once('value')

    }).then(snapshot=>{
        expect(snapshot.val().amount).toBe(updates.amount);
        done();
    });

});


test('should return remove action object', () => {
    const action = removeExpense({id: "123abc"});
    expect(action).toEqual({type: "REMOVE_EXPENSE", id: "123abc"});
});


test('should remove expense by id from firebase', (done) => {
    const mockStore = createMockStore(defaultAuthState);
    const id = expenses[0].id;
    mockStore.dispatch(startRemoveExpense({id}))
        .then(() => {
            const actions = mockStore.getActions();
            expect(actions[0]).toEqual({
                type: "REMOVE_EXPENSE",
                id: id
            });
            return database.ref(`users/${uid}/expenses/${id}`).once('value')
        }).then((snapshot) => {
        expect(snapshot.val()).toBeFalsy();
        done();
    });
});
