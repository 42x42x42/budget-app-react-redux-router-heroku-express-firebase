import filterReducer from '../../../redux/reducers/filters-Reducers';
import moment from 'moment';

const filterDefaultState = {
    text: '',
    sortBy: 'date',
    startDate: moment().startOf('month'),
    endDate: moment().endOf('month')
};

test('init function sets default state', () => {
    const state = filterReducer(undefined, {type: '@@INIT'});
    expect(state).toEqual(filterDefaultState);
});

test('sort by amount', () => {
    const state = filterReducer(undefined, {type: 'SORT_BY_AMOUNT'});
    expect(state.sortBy).toBe('amount');
});

test('sort by date', () => {
    const action = {type: 'SORT_BY_DATE'};
    const defState = {
        ...filterDefaultState,
        sortBy: 'amount'
    };
    const state = filterReducer(defState, action);
    expect(state.sortBy).toBe('date');
});

test('set TEXT filter', () => {
    const state = filterReducer(undefined, {type: 'SET_TEXT_FILTER', text: 'test'});
    expect(state.text).toBe('test');
});

test('set START date filter', () => {
    const startDate = moment();
    const state = filterReducer(undefined, {
        type: 'SET_START_DATE',
        startDate
    });
    expect(state.startDate).toBe(startDate);
});

test('set END date filter', () => {
    const endDate = moment();
    const state = filterReducer(undefined, {
        type: 'SET_END_DATE',
        endDate
    });
    expect(state.endDate).toBe(endDate);
});