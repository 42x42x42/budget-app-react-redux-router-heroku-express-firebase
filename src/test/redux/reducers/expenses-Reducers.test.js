import moment from "moment";
import expensesReducer from '../../../redux/reducers/expenses-Reducer';
import defaultExpenses from '../../fixtures/expenses';

test('INIT exp reducer', () => {
    const state = expensesReducer(undefined, {type: '@@INIT'});
    expect(state).toEqual([])
});

test('should REMOVE expense by id', () => {
    const action = {type: 'REMOVE_EXPENSE', id: defaultExpenses[1].id};
    const state = expensesReducer(defaultExpenses, action);
    expect(state).toEqual([defaultExpenses[0], defaultExpenses[2]]);
});

test('should NOT REMOVE expense with incorrect id', () => {
    const action = {type: 'REMOVE_EXPENSE', id: 'incorrect id'};
    const state = expensesReducer(defaultExpenses, action);
    expect(state).toEqual(defaultExpenses);
});

test('should ADD expense', () => {
    const expense = {
        description: 'added expense',
        id: '14',
        note: 'final',
        amount: 100,
        createdAt: moment()
    };
    const action = {
        type: "ADD_EXPENSE",
        expense: expense
    };
    const state = expensesReducer(defaultExpenses, action);
    expect(state[3]).toEqual(expense);
});

test('should EDIT existing expense', () => {
    const action = {
        type: "EDIT_EXPENSE",
        id: defaultExpenses[0].id,
        updates: {description: 'updated: car part'}
    };
    const state = expensesReducer(defaultExpenses, action);
    expect(state[0].description).toBe('updated: car part');
});

test('should NOT EDIT existing expense due to incorrect id', () => {
    const action = {
        type: "EDIT_EXPENSE",
        id: 'incorrect id',
        updates: {description: 'updated'}
    };
    const state = expensesReducer(defaultExpenses, action);
    expect(state).toEqual(defaultExpenses);
});

test('should set expenses',()=>{
    const action = {
        type:"SET_EXPENSES",
        expenses: defaultExpenses[1]
    };
    const state = expensesReducer(defaultExpenses,action);
    expect(state).toEqual(defaultExpenses[1]);

});