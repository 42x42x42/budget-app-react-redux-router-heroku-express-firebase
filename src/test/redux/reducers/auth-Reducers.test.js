import authReducer from '../../../redux/reducers/auth-Reducer';

test("should set uid for login", () => {
    const action = {
        type: "LOGIN",
        uid: '123abc'
    };
    const state = authReducer({}, action);
    expect(state.uid).toBe(action.uid);
});

test("should wipe uid", () => {
    const action = {type: "LOGOUT"};
    const state = authReducer({uid: "123abc"}, action);
    let a = 1;
    expect(state).toEqual({});
});
