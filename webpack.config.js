const path = require('path');
const MiniCSSExtractPlugin = require("mini-css-extract-plugin");
const webpack = require('webpack');

process.env.NODE_ENV = process.env.NODE_ENV || 'development';
if (process.env.NODE_ENV === 'test') {
    console.log('______MODE', process.env.NODE_ENV);
    require('dotenv').config({path: ".env.test"});
} else if (process.env.NODE_ENV === 'development') {
    require('dotenv').config({path: ".env.development"});
}


module.exports = (env) => {
    const isProduction = env === 'production';

    return {
        entry: ['babel-polyfill','./src/app.js'],
        output: {
            path: path.join(__dirname, 'public', 'dist'),
            filename: "bundle.js"
        },
        module: {
            rules: [{
                loader: 'babel-loader',
                test: /\.js$/,
                exclude: /node_modules/
            },
                {
                    test: /\.s?css$/,
                    use: [
                        MiniCSSExtractPlugin.loader,
                        {
                            loader: "css-loader",
                            options: {
                                sourceMap: true
                            }
                        },
                        {
                            loader: "sass-loader",
                            options: {
                                sourceMap: true
                            }
                        }
                    ]
                }]
        },
        plugins: [
            new MiniCSSExtractPlugin({filename: "style.css"}),
            new webpack.DefinePlugin({
                'process.env.FIREBASE_API_KEY': JSON.stringify(process.env.FIREBASE_API_KEY),
                'process.env.FIREBASE_AUTH_DOMAIN': JSON.stringify(process.env.FIREBASE_AUTH_DOMAIN),
                'process.env.FIREBASE_DATABASE_URL': JSON.stringify(process.env.FIREBASE_DATABASE_URL),
                'process.env.FIREBASE_PROJECT_ID': JSON.stringify(process.env.FIREBASE_PROJECT_ID),
                'process.env.FIREBASE_STORAGE_BUCKET': JSON.stringify(process.env.FIREBASE_STORAGE_BUCKET),
                'process.env.FIREBASE_MESSAGING_SENDER_ID': JSON.stringify(process.env.FIREBASE_MESSAGING_SENDER_ID)
            })
        ],
        // devtool: isProduction ? 'source-map' : 'cheap-module-eval-source-map', работает быстрее,но не показывает оригинальный css
        devtool: isProduction ? 'source-map' : 'inline-source-map',
        devServer: {
            contentBase: path.join(__dirname, 'public'),
            historyApiFallback: true,
            publicPath: '/dist/'
        }
    };
};

//module настраивает трансформацию через бабель перед тем как вебпак создаст bundle.js

// devtool исправляет отображение ошибок в консоли хроме и делает ссылку на оригинальный код,а не bundle.js
// >> 'cheap-module-eval-source-map' Для разработки

/* настройка девсерва, нужно так же сделать команду в Package json, упуть указывается к файлу index.html, path импортируется
devServer :{
contentBase: path.join(__dirname, 'public')
}
 devServer не генерирует физически файл bundle.js он хранит все в оперативной памяти
*/

//babel-plugin-transform-class-properties позволяет избежать необходимости
// в конструкторе писать this.eventHandler = this.eventHandler.bind(this),
// можно сразу через foo = (e) => {..} писать в классе без конструктора и this всегда будет объект в котором функция объявлена
// переменные в классе можно писать без типа let\var а сразу так: name = 'Mike' . это равнозначно в конструкторе this.name='Mike

//npm install --save-dev babel-plugin-transform-object-rest-spread
//+ webpack config "plugins": ["transform-object-rest-spread"]
// позволяет использовать ... для объхектов

// css-loader загружает css в js script
// style-loader Загружате стиль в каждый элемент в дом в тег style
// в настройках нужно добавить код отдельным объектом в раздел модули-> rules
//use позволяет указать сразу несколько лоадеров
// сам css файл нужно просто импортировать в файл с финальным компонентом(app) import './styles/styles.scss';
/*
{
    test: /\.css$/,
        use:[
    'style-loader',
    'css-loader'
]
}]
*/


/* SCSS нужно установить sass-loader node-sass
нужно в test указать файл .scss
Знак вопроса /\.s?css$/ означает что будут браться в работу css и scss файлы. ? означает необязательность предыдущего символа
{
                test: /\.s?css$/,
                use:[
                    'style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            }]

 */

// npm install normalize.css нормализирует css
// import 'normalize.css/normalize.css'; в основном файле приложения + см выше настройки вебпак конфига. Не забыть перезапустить сервер

//react-dates@12.7.0 (!!именно эта версия)для работы с календарем, библиотека от эирбнб,требует так же установить npm i react-addons-shallow-compare
// и импортировать import 'react-dates/lib/css/_datepicker.css';

// moments - библиотека для работы с датой\временем

// jest для тестирования
// react-test-renderer для тестового рендеринга компонентов реакта

//npm i --save-dev enzyme enzyme-adapter-react-16 raf - ензим для тестирование компонентов
// (+см настройки в 3х файлах(package.json,jest.config.json + new file inside folder test setupTest.js
//npm i enzyme-to-json для работы со снепшотами, чтоб ензим не добавлял туда кучу своей лишней инфомрации

//packge.json "build:prod": "webpack -p --env production",
// --env production передает в вебпак конфиг переменную env со значением production. -p обозначение для продакшена