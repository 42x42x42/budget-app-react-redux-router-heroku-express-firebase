const express = require('express');
const path = require('path');
const port = process.env.PORT || 3000; // check if there is a port running by heroku, if not set local port 3000

const app = express();
const publicPath = path.join(__dirname,'..','public');

app.use(express.static(publicPath));

// always return index.html (because all routing is there)
app.get('*',(request,response)=>{
    response.sendFile(path.join(publicPath,'index.html'));
});

app.listen(port,()=>{
    console.log('Server is up!');
});

// to launch type in command line: node server/server